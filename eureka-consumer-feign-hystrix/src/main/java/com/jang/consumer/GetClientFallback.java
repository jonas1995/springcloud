package com.jang.consumer;

import org.springframework.stereotype.Component;

/**
 * title GetClientFallback
 * projectName springcloud
 * desc Class
 *
 * @author jiangjian
 * @date 2020/8/21 16:34
 */
@Component
public class GetClientFallback implements GetClient {
    @Override
    public String getService() {
        return "fallback";
    }
}
