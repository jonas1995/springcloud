package com.jang.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * title GetController
 * projectName springcloud
 * desc Class
 *
 * @author jiangjian
 * @date 2020/8/21 16:37
 */
@RestController
public class GetController {

    @Autowired
    private GetClient getClient;

    @GetMapping("getService")
    public String getService() {
        return getClient.getService();
    }

}
