package com.jang.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jang
 */
@EnableDiscoveryClient
@SpringBootApplication
@RestController
public class EurekaProviderApplication {

    @Autowired
    private DiscoveryClient discoveryClient;

    public static void main(String[] args) {
        SpringApplication.run(EurekaProviderApplication.class, args);
    }

    @GetMapping("getServices")
    public String servers() throws InterruptedException {
//        Thread.sleep(5000L);
        return "Services:" + discoveryClient.getServices();
    }

}
