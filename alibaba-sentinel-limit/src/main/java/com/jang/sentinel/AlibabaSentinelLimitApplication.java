package com.jang.sentinel;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jang
 */
@SpringBootApplication
public class AlibabaSentinelLimitApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlibabaSentinelLimitApplication.class, args);
    }

    @RestController
    static class TestController {

        @GetMapping("hello")
        @SentinelResource(fallback = "callback")
        public String hello() {
            return "Hello Success";
        }

        public String callback() {
            return "Hello Callback";
        }

    }

}
