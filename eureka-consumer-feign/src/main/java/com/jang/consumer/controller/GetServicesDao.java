package com.jang.consumer.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * title GetServicesDao
 * projectName springcloud
 * desc 获取服务接口
 *
 * @author jiangjian
 * @date 2020/4/26 15:00
 */
@FeignClient("eureka-provider")
public interface GetServicesDao {

    /**
     * 获取服务
     *
     * @return String
     * @author jiangjian
     * @date 2020/04/26
     */
    @GetMapping("getServices")
    String getServices();

}
