package com.jang.consumer;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * title GetController
 * projectName springcloud
 * desc getService
 *
 * @author jiangjian
 * @date 2020/8/19 14:43
 */
@RestController
public class GetController {

    @Autowired
    private ConsumerService consumerService;

    @GetMapping("getService")
    public String getService() {
        return consumerService.getService();
    }

    @Service
    class ConsumerService {

        @Autowired
        private RestTemplate restTemplate;

        @HystrixCommand(fallbackMethod = "fallBack")
        public String getService() {
            return restTemplate.getForObject("http://eureka-provider/getServices", String.class);
        }

        private String fallBack() {
            return "fallBack";
        }

    }

}
