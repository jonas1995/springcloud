package com.jang.consumer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * title GetClient
 * projectName springcloud
 * desc Interface
 *
 * @author jiangjian
 * @date 2020/8/21 16:32
 */
@FeignClient(value = "eureka-provider", fallback = GetClientFallback.class)
@Component
public interface GetClient {

    /**
     * 获取服务
     *
     * @return String
     */
    @GetMapping("getServices")
    String getService();

}
