package com.jang.stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Jang
 */
@SpringBootApplication
public class StreamHelloApplication {

    public static void main(String[] args) {
        SpringApplication.run(StreamHelloApplication.class, args);
    }

}
