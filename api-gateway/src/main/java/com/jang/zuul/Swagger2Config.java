package com.jang.zuul;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * title Swagger2Config
 * projectName springcloud
 * desc Swagger2配置
 *
 * @author jiangjian
 * @date 2020/8/26 16:42
 */
@Component
@Primary
public class Swagger2Config implements SwaggerResourcesProvider {
    @Override
    public List<SwaggerResource> get() {
        List<SwaggerResource> resources = new ArrayList<>();
        resources.add(swaggerResource("service-a", "/swagger-service-a/v2/api-docs?accessToken=123", "2.0"));
        resources.add(swaggerResource("service-b", "/swagger-service-b/v2/api-docs?accessToken=123", "2.0"));
        return resources;
    }

    private SwaggerResource swaggerResource(String name, String location, String version) {
        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(version);
        return swaggerResource;
    }
}
