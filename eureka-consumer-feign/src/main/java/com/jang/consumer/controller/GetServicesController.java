package com.jang.consumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * title GetServicesController
 * projectName springcloud
 * desc 获取服务控制器
 *
 * @author jiangjian
 * @date 2020/4/26 14:59
 */
@RestController
public class GetServicesController {

    @Autowired
    private GetServicesDao getServicesDao;

    @GetMapping("consumer")
    public String consumer() {
        return getServicesDao.getServices();
    }

}
