package com.jang.nacos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author Jang
 */
@EnableDiscoveryClient
@SpringBootApplication
public class AlibabaNacosDiscoveryClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlibabaNacosDiscoveryClientApplication.class, args);
    }


    @RestController
    static class TestController {

        @Autowired
        private LoadBalancerClient loadBalancerClient;

        @GetMapping("hello")
        public String hello() {

            /**
             * 支持以下几种服务消费方式
             * RestTemplate：和eureka消费方式一样
             * Ribbon：和eureka消费方式一样
             * Feign：和eureka消费方式一样
             * WebClient：
             *   @RestController
             *   static class TestController {
             *
             *       @Autowired
             *       private WebClient.Builder webClientBuilder;
             *
             *       @GetMapping("/test")
             *       public Mono<String> test() {
             *           Mono<String> result = webClientBuilder.build()
             *                   .get()
             *                   .uri("http://alibaba-nacos-discovery-server/hello?name=Jang")
             *                   .retrieve()
             *                   .bodyToMono(String.class);
             *           return result;
             *       }
             *   }
             *
             *   @Bean
             *   @LoadBalanced
             *   public WebClient.Builder loadBalancedWebClientBuilder() {
             *       return WebClient.builder();
             *   }
             */

            ServiceInstance instance = loadBalancerClient.choose("alibaba-nacos-discovery-server");

            String url = instance.getUri() + "/hello?name=Jang";

            RestTemplate template = new RestTemplate();

            String result = template.getForObject(url, String.class);

            return "Invoke : " + url + ", return : " + result;

        }

    }

}
