package com.jang.swagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author Jang
 */
@EnableDiscoveryClient
@SpringBootApplication
public class SwaggerServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SwaggerServiceApplication.class, args);
    }

}
