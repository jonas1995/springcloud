package com.jang.client;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * title TestController
 * projectName springcloud
 * desc test
 *
 * @author jiangjian
 * @date 2020/8/18 15:24
 */
@RefreshScope
@RestController
public class TestController {

    @Value("${from}")
    private String from;

    @GetMapping("from")
    public String from() {
        return this.from;
    }

}
