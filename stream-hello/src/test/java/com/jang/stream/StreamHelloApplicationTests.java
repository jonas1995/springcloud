package com.jang.stream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@SpringBootTest
@EnableBinding(StreamHelloApplicationTests.SinkSender.class)
class StreamHelloApplicationTests {

    @Autowired
    private SinkSender sinkSender;

    @Test
    void sinkSenderTester() {
        sinkSender.output().send(MessageBuilder.withPayload("produce a message").build());
    }

    @Component
    public interface SinkSender {
        String OUTPUT = "input";

        @Output(SinkSender.OUTPUT)
        MessageChannel output();
    }

}
