package com.jang.swagger.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * title ServiceController
 * projectName springcloud
 * desc 服务
 *
 * @author jiangjian
 * @date 2020/8/26 15:54
 */
@RestController
public class ServiceController {

    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping("service-a")
    @ApiOperation(value = "获取注册中心服务列表")
    public String getServicesA(@RequestParam("accessToken") String accessToken) {
        return "Services: " + discoveryClient.getServices();
    }

}
