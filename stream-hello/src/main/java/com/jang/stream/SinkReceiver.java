package com.jang.stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

/**
 * title SinkReceiver
 * projectName springcloud
 * desc RabbitMQ消息的消费者
 *
 * @author jiangjian
 * @date 2020/8/28 16:58
 */
@EnableBinding(Sink.class)
public class SinkReceiver {

    private static Logger log = LoggerFactory.getLogger(SinkReceiver.class);

    @StreamListener(Sink.INPUT)
    public void receiver(Object payload) {
        log.info("Received: " + payload);
    }

}
